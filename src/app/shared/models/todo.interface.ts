export interface Todo {
  content: string;
  done: boolean;
  startDate: Date;
}
