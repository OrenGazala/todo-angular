import { Component, OnInit } from '@angular/core';
import { Todo } from '../shared/models/todo.interface';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  todos: Todo[];
  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.dataService.getData().subscribe((todos) => {
      this.todos = todos;
    });
  }
}
