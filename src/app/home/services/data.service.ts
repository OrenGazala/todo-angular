import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Todo } from 'src/app/shared/models/todo.interface';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor() {}

  getData(): Observable<Todo[]> {
    const todos: Todo[] = [
      {
        content: 'check',
        done: false,
        startDate: new Date(),
      },
      {
        content: 'check2',
        done: false,
        startDate: new Date(),
      },
    ];
    // return this.httpService.get<Todo[]>('/');
    return of(todos);
  }
}
